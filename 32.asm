INCLUDE Irvine32.inc
.data
Vopros  byte  "Jelaesi stati programistom?-[y/n]",0
Da  DB  10,13,'Stanesi !',10,13,0
Net  DB  10,13,'Stanesi filosofom!',0ah,0dh,0
Err BYTE 10,13,"Jmi pravilino clavisu !",7,0
.code
main PROC

mov edx,OFFSET Vopros 
call WriteString

er: 
call ReadChar

cmp al,'y' 
jz IsDad
cmp al,'n' 
jz IsNud 
mov edx,OFFSET Err 
call WriteString
jmp er 

IsDad: mov edx,OFFSET Da 
call WriteString
jmp ex
IsNud: mov edx,OFFSET Net 
call WriteString

ex:
	exit
main ENDP
END main
